﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanController : Controller {

    public Transform tf;
    public float speed;
    public FollowPlayerCamera FPC;

    public float hoverForce;
    public float hoverTime;
    private float maxHoverTime;
    public Text HoverTimeText;

    public float lives = 3;
    public Text LifeText;

    public float score;
    public Text ScoreText;

    [HideInInspector] public GameManager gm;
    public GameObject playerPrefab;
    public GameObject playerRespawnPoint;


    public bool isGrounded;
    public float groundDistance = 0.1f;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        tf = GetComponent<Transform>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        setLifeText();
        setPawn();
        gm.volumeAudio.clip = gm.audioClips[1];
        gm.volumeAudio.loop = true;
        gm.volumeAudio.Play();
        maxHoverTime = hoverTime;
        setLifeText();
        SetHoverTime();
        SetScore(0);
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movementVector = Vector2.zero;
        //move straight
        if (Input.GetKey(KeyCode.W))
        {
            if (hoverTime > 0)
            {
                isGrounded = false;
                pawn.rb.AddForce(Vector2.up * hoverForce, ForceMode2D.Force);
            }
            else if (hoverTime <= 0)
            {
                hoverTime = 0;
            }
            SetHoverTime();
        }
        //rotate left
        if (Input.GetKey(KeyCode.A))
        {
            movementVector.x = -1;
        }
        //rotate right
        if (Input.GetKey(KeyCode.D))
        {
            movementVector.x = 1;
        }
        //move backwards
        if (Input.GetKey(KeyCode.S))
        {
            if (isGrounded == false)
            {
                if (hoverTime > 0)
                {
                    pawn.rb.AddForce(Vector2.down * hoverForce, ForceMode2D.Force);
                    hoverTime -= Time.deltaTime;
                }
                else if (hoverTime <= 0)
                {
                    hoverTime = 0;
                }
                SetHoverTime();
            }
        }
        pawn.Move(movementVector);
        if (lives <= 0)
        {
            SceneManager.LoadScene(3);
        }
        if (isGrounded == false)
        {
            if (hoverTime > 0)
            {
                hoverTime -= Time.deltaTime;
            }
            if (hoverTime <= 0)
            {
                hoverTime = 0;
            }
            SetHoverTime();
        }
        if (isGrounded == true)
        {
            if (hoverTime < maxHoverTime)
            {
                hoverTime += Time.deltaTime;
            }
            if (hoverTime >= maxHoverTime)
            {
                hoverTime = maxHoverTime;
            }
            SetHoverTime();
        }
    }

    //checking for grounded using raycast
    //public void CheckForGrounded()
    //{
    //    RaycastHit2D hitInfo = Physics2D.Raycast(pawn.tf.position, Vector2.down, groundDistance);
    //    if (hitInfo.collider == null)
    //    {
    //        isGrounded = false;
    //    }
    //    else
    //    {
    //        isGrounded = true;
    //        Debug.Log("Standing on " + hitInfo.collider.name);
    //    }
    //}

    public void setLifeText()
    {
        LifeText.text = "Lives Left: " + lives.ToString();
    }

    public void InstantiatePlayer()
    {
        Instantiate(playerPrefab, playerRespawnPoint.transform.position, Quaternion.identity);
    }

    public void setPawn()
    {
        gm.playerPawn = pawn;
    }

    public void SetScore(float flowerCost)
    {
        score = score + flowerCost;
        gm.playerScore = score;
        ScoreText.text = "Flowers Collected: " + score.ToString();
    }

    public void SetHoverTime()
    {
        HoverTimeText.text = "Hover Time Left: " + hoverTime.ToString();
    }
}
