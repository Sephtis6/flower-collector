﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{

    [HideInInspector] public HumanController CC;
    [HideInInspector] public Transform tf;

    //integers and floats named
    [HideInInspector] public Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;

    public void Start()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        CC.pawn = GetComponent<Pawn>();
        tf = GetComponent<Transform>();
        CC.setPawn();

        // Get my components
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    public void Update()
    {

    }

    public void Move(Vector2 moveVector)
    {
        // Change X velocity based on speed and moveVector
        rb.velocity = new Vector2(moveVector.x * CC.speed, rb.velocity.y);

        // If velocity is <0, flip sprite. 
        if (rb.velocity.x < 0)
        {
            sr.flipX = true;
        }
        // If velocity is >0, flip sprite. 
        else if (rb.velocity.x > 0)
        {
            sr.flipX = false;
        }
    }
}