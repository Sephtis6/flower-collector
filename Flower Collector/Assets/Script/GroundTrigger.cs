﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTrigger : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Pawn pawn = collision.gameObject.GetComponent<Pawn>();
        HumanController CC = pawn.CC;
        CC.isGrounded = true;
    }
}
