﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SuccessScript : MonoBehaviour
{

    public float ExitScore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        HumanController CC = collision.gameObject.GetComponent<Pawn>().CC;
        if (CC.score <= ExitScore)
        {
            SceneManager.LoadScene(3);
        }
        if (CC.score >= ExitScore)
        {
            SceneManager.LoadScene(4);
        }
    }
}
